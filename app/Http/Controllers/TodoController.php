<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\TodoRequest;
use App\Models\Todo;

class TodoController extends Controller
{
    public function index(TodoRequest $req){
     $task = new Todo();
     $task->task = $req->input('task');
     $task->save();

     return redirect()->route('allTask');
    }
    public function getTask(){
        $tasks = Todo::all();
        return view('todo.index', ['tasks' => $tasks]);
    }
    public function deleteTask($id){
        Todo::find($id)->delete();
        return redirect()->route('allTask');
    }
    public function updateTask($id,TodoRequest $req){
        $task= Todo::find($id);
        $task->task = $req->input('task');
        $task->save();
        return redirect()->route('allTask');
    }
}
