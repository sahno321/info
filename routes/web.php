<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoController;


Route::post('/todo',[TodoController::class, 'index'])->name('addTask');
Route::get('/all',[TodoController::class, 'getTask'])->name('allTask');
Route::get('/delete{id}',[TodoController::class,'deleteTask'])->name('deleteTask');
Route::get('/update{id}',[TodoController::class,'updateTask'])->name('updateTask');

