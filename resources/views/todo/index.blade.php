<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task Manager</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
        }
        h1 {
            font-size: 24px;
            margin-bottom: 10px;
        }
        .task-form {
            display: flex;
            align-items: center;
            margin-bottom: 20px;
        }
        .task-form input[type="text"] {
            flex: 1;
            padding: 5px;
        }
        .task-form button {
            margin-left: 10px;
        }
        .task-list {
            list-style: none;
            padding: 0;
        }
        .task-item {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
        }
        .task-item input[type="text"] {
            flex: 1;
            padding: 5px;
        }
        .task-item button {
            margin-left: 10px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Add Task</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('addTask') }}" method="post" class="task-form">
        @csrf
        <label>
            <input type="text" name="task" placeholder="Create task" class="form-control">
        </label>
        <button type="submit" class="btn btn-success">Add</button>
    </form>
    <h2>Tasks</h2>
    <ul class="task-list">
        @foreach ($tasks as $task)
            <li class="task-item">
                <form action="{{ route('updateTask', $task->id) }}" method="POST">
                    @csrf
                    @method('GET')
                    <label>
                        <input type="text" name="task" value="{{ $task->task }}" class="form-control">
                    </label>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
                <a href="{{ route('deleteTask', $task->id) }}">
                    <button class="btn btn-danger">Delete</button>
                </a>
            </li>
        @endforeach
    </ul>
</div>
</body>
</html>
